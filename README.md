# README #

Test challenge based on the marvel APIs

### Characteristics of the project ###

* Adaptive icons
* Using external fonts
* Using MVP architecture
* Using dependency Injection
* Transitions between screens
* Mixing Java and Kotlin

### Libraries ###
* Retrofit
* RxJava2
* Dagger2
* Databinding
* Glide
* GSON
* okhttp3
* Android support libraries
* Test libraries like mockito, espresso, ...

### Tests ###
* Add unit test for presenters and repositories
* Add some automation test with espresso around the search screen

./gradlew clean testDebugUnitTest connectedDebugAndroidTest createDebugCoverageReport

### Who do I talk to? ###

* Ferran Ribell Bachs