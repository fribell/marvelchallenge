package com.ribell.ferran.opentrends.ui.splashscreen;


import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.ribell.ferran.opentrends.R;
import com.ribell.ferran.opentrends.ui.search.SearchActivity;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class SearchActivityTest {

    private static final int SPLASH_SCREEN_WAIT = 2100;
    private static final int LOAD_RECYCLERVIEW = 2500;
    @Rule
    public ActivityTestRule<SplashScreen> splashActivityTestRule = new ActivityTestRule<>(SplashScreen.class);

    @Rule
    public ActivityTestRule<SearchActivity> searchActivityTestRule = new ActivityTestRule<>(SearchActivity.class);

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }

    @Test
    public void searchActivityTest() {

        setSleep(SPLASH_SCREEN_WAIT);
        typeSearchText("spi");
        setSleep(LOAD_RECYCLERVIEW);

        ViewInteraction imageView = onView(
                allOf(withId(R.id.image),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.list),
                                        0),
                                0),
                        isDisplayed()));
        imageView.check(matches(isDisplayed()));
    }

    @Test
    public void searchActivityEmptyTest() {

        setSleep(SPLASH_SCREEN_WAIT);
        typeSearchText("Ferran");
        setSleep(LOAD_RECYCLERVIEW);

        onView(withId(R.id.empty_results))
                .check(matches(withText(R.string.empty_results)));
    }

    private void typeSearchText(String search) {
        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.search),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                1),
                        isDisplayed()));

        appCompatEditText.perform(replaceText(search), closeSoftKeyboard());
    }

    private void setSleep(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void clickItemTest() {
        setSleep(SPLASH_SCREEN_WAIT);
        typeSearchText("spi");
        setSleep(LOAD_RECYCLERVIEW);

        onView(withId(R.id.list))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

        onView(withId(R.id.name))
                .check(matches(withText("Spider-dok")))
                .check(matches(isDisplayed()));
    }
}
