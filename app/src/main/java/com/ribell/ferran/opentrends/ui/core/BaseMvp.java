package com.ribell.ferran.opentrends.ui.core;

public interface BaseMvp {
    interface BaseView {

    }

    interface BasePresenter<V extends BaseView> {

        void attachView(V view);

        void detachView();

    }
}
