package com.ribell.ferran.opentrends.net.services;

import com.ribell.ferran.opentrends.net.responses.HeroApiResponse;
import com.ribell.ferran.opentrends.net.responses.OtherApiResponse;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MarvelService {
    @GET("/v1/public/characters")
    Single<Response<HeroApiResponse>> searchHero(
            @Query("nameStartsWith") String name
    );

    @GET("/v1/public/characters/{heroId}/events")
    Single<Response<OtherApiResponse>> getEvents(
            @Path("heroId") String heroId
    );

    @GET("/v1/public/characters/{heroId}/comics")
    Single<Response<OtherApiResponse>> getComics(
            @Path("heroId") String heroId
    );
}
