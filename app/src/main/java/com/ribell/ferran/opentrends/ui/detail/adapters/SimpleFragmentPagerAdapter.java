package com.ribell.ferran.opentrends.ui.detail.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;

import com.ribell.ferran.opentrends.R;
import com.ribell.ferran.opentrends.ui.detail.OthersFragment;

public class SimpleFragmentPagerAdapter extends FragmentStatePagerAdapter {

    private final Context context;
    private final SparseArray<OthersFragment> fragmentHashMap;

    public SimpleFragmentPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.context = context;
        fragmentHashMap = new SparseArray<>();
    }

    @Override
    public Fragment getItem(int position) {
        OthersFragment fragment = OthersFragment.newInstance();
        fragmentHashMap.put(position, fragment);
        return fragment;
    }

    public OthersFragment getFragment(int position) {
        return fragmentHashMap.get(position);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return context.getString(R.string.comics);
            case 1:
                return context.getString(R.string.events);
            default:
                return null;
        }
    }

}
