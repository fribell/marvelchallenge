package com.ribell.ferran.opentrends.ui.detail;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.ribell.ferran.opentrends.MyApplication;
import com.ribell.ferran.opentrends.R;
import com.ribell.ferran.opentrends.databinding.ActivityDetailBinding;
import com.ribell.ferran.opentrends.models.Hero;
import com.ribell.ferran.opentrends.models.Other;
import com.ribell.ferran.opentrends.models.Urls;
import com.ribell.ferran.opentrends.ui.detail.adapters.SimpleFragmentPagerAdapter;

import java.util.List;

import javax.inject.Inject;

import static com.ribell.ferran.opentrends.models.HeroKt.HERO_TAG;
import static com.ribell.ferran.opentrends.models.UrlsKt.URL_COMIC;
import static com.ribell.ferran.opentrends.models.UrlsKt.URL_DETAIL;
import static com.ribell.ferran.opentrends.models.UrlsKt.URL_WIKI;

public class DetailActivity extends AppCompatActivity implements DetailMvp.View {


    public static final int COMICS = 0;
    public static final int EVENTS = 1;

    private ActivityDetailBinding binding;
    private Hero hero;
    private SimpleFragmentPagerAdapter simpleFragmentPagerAdapter;

    @SuppressWarnings("WeakerAccess")
    @Inject
    DetailPresenter presenter;

    public static Intent newIntent(Context context, Hero hero) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra(HERO_TAG, hero);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        MyApplication.getInstance().getPresenterComponent().inject(this);
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail);
        presenter.attachView(this);
        setActivityTransition();

        hero = (Hero) getIntent().getSerializableExtra(HERO_TAG);
        binding.setHero(hero);

        setPagerAdapter();
        setBackNavigation();
        setButtonLinks();

        presenter.loadComicsData(hero.getId());
        presenter.loadEventsData(hero.getId());
    }

    private void setBackNavigation() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.super_hero);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setButtonLinks() {
        for (Urls url : hero.getUrls()) {
            Button button = null;
            switch (url.getType()) {
                case URL_DETAIL:
                    button = binding.details;
                    break;
                case URL_WIKI:
                    button = binding.wiki;
                    break;
                case URL_COMIC:
                    button = binding.comics;
                    break;
            }
            if (button != null) {
                button.setVisibility(View.VISIBLE);
                button.setOnClickListener(v -> {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url.getUrl()));
                    startActivity(browserIntent);
                });
            }
        }
    }

    private void setPagerAdapter() {
        simpleFragmentPagerAdapter =
                new SimpleFragmentPagerAdapter(this, getSupportFragmentManager());

        binding.viewpager.setAdapter(simpleFragmentPagerAdapter);
        binding.tabs.setupWithViewPager(binding.viewpager);
    }

    private void setActivityTransition() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            binding.heroDetail.image.setTransitionName(getString(R.string.image_transition));
            binding.heroDetail.name.setTransitionName(getString(R.string.name_transition));
            binding.heroDetail.description.setTransitionName(getString(R.string.description_transition));
        }
    }

    @Override
    public void displayComicResults(List<Other> results) {
        OthersFragment fragment = simpleFragmentPagerAdapter.getFragment(EVENTS);
        if (fragment != null) {
            fragment.loadData(results);
        }
    }

    @Override
    public void displayEventsResults(List<Other> results) {
        OthersFragment fragment = simpleFragmentPagerAdapter.getFragment(COMICS);
        if (fragment != null) {
            fragment.loadData(results);
        }
    }

    @Override
    public void displayError(String errorMessage) {
        Snackbar.make(binding.tabs, errorMessage, Snackbar.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }
}
