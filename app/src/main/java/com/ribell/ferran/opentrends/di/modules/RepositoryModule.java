package com.ribell.ferran.opentrends.di.modules;

import com.ribell.ferran.opentrends.net.services.MarvelService;
import com.ribell.ferran.opentrends.repositoris.MarvelRepository;
import com.ribell.ferran.opentrends.repositoris.MarvelRepositoryAPI;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = {
        NetModule.class}
)
public class RepositoryModule {

    @Provides
    @Singleton
    MarvelRepository provideMarvelRepository(MarvelService marvelService) {
        return new MarvelRepositoryAPI(marvelService);
    }

}
