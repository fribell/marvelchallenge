package com.ribell.ferran.opentrends.models

import java.io.Serializable

const val HERO_TAG: String = "HERO_TAG"

data class Hero(var id: String,
                var name: String,
                var description: String,
                var thumbnail: Thumbnail, val urls: List<Urls>) : Serializable