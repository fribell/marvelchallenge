package com.ribell.ferran.opentrends.di.components;

import com.ribell.ferran.opentrends.di.modules.NetModule;
import com.ribell.ferran.opentrends.di.modules.PresenterModule;
import com.ribell.ferran.opentrends.di.modules.RepositoryModule;
import com.ribell.ferran.opentrends.di.scopes.ActivityScope;
import com.ribell.ferran.opentrends.ui.detail.DetailActivity;
import com.ribell.ferran.opentrends.ui.search.SearchActivity;

import javax.inject.Singleton;

import dagger.Component;


@ActivityScope
@Singleton
@Component(modules = {
        PresenterModule.class,
        NetModule.class,
        RepositoryModule.class
})
public interface PresenterComponent {

    void inject(SearchActivity activity);

    void inject(DetailActivity activity);

}
