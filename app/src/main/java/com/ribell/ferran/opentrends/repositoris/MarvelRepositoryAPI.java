package com.ribell.ferran.opentrends.repositoris;

import com.ribell.ferran.opentrends.models.Hero;
import com.ribell.ferran.opentrends.models.Other;
import com.ribell.ferran.opentrends.net.responses.OtherApiResponse;
import com.ribell.ferran.opentrends.net.services.MarvelService;
import com.ribell.ferran.opentrends.ui.detail.DetailActivity;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import retrofit2.Response;

public class MarvelRepositoryAPI implements MarvelRepository {
    private final MarvelService service;

    @Inject
    public MarvelRepositoryAPI(MarvelService service) {
        this.service = service;
    }

    @Override
    public Single<List<Hero>> searchHero(String name) {
        return service.searchHero(name)
                .flatMap(marvelApiResponse -> {
                            if (marvelApiResponse.isSuccessful() && marvelApiResponse.body() != null) {
                                return Single.just(marvelApiResponse.body().getResults());
                            } else {
                                return Single.error(new Throwable(marvelApiResponse.message()));
                            }
                        }
                );
    }

    @Override
    public Single<List<Other>> loadData(int type, String heroId) {
        Single<Response<OtherApiResponse>> result;
        if (type == DetailActivity.EVENTS) {
            result = service.getEvents(heroId);
        } else {
            result = service.getComics(heroId);
        }

        return result.flatMap(marvelApiResponse -> {
            if (marvelApiResponse.isSuccessful() && marvelApiResponse.body() != null) {
                        return Single.just(marvelApiResponse.body().getResults());
                    } else {
                        return Single.error(new Throwable(marvelApiResponse.message()));
                    }
                }
        );
    }
}
