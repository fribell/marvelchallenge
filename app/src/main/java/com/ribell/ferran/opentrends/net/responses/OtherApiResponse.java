package com.ribell.ferran.opentrends.net.responses;

import com.ribell.ferran.opentrends.models.Other;

import java.util.ArrayList;
import java.util.List;

/*
 The response of the API and the data that I need to show in the comics and events are the same so
 I reuse the objects.
 */
public class OtherApiResponse {

    private DataResponse data;

    private class DataResponse {
        private int count;

        private List<Other> results;

        public List<Other> getResults() {
            return results;
        }
    }

    public List<Other> getResults() {
        return data.count > 0 ? data.getResults() : new ArrayList<>();
    }
}
