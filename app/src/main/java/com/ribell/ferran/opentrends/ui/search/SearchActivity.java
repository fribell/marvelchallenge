package com.ribell.ferran.opentrends.ui.search;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.ribell.ferran.opentrends.MyApplication;
import com.ribell.ferran.opentrends.R;
import com.ribell.ferran.opentrends.databinding.ActivitySearchBinding;
import com.ribell.ferran.opentrends.models.Hero;
import com.ribell.ferran.opentrends.ui.detail.DetailActivity;
import com.ribell.ferran.opentrends.ui.search.adapters.HeroAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import static com.ribell.ferran.opentrends.models.HeroKt.HERO_TAG;
import static com.ribell.ferran.opentrends.utils.UtilsKt.hideSoftKeyboard;

public class SearchActivity extends AppCompatActivity implements SearchMvp.View {

    private static final int SEARCH_LIMIT = 2;
    @SuppressWarnings("WeakerAccess")
    @Inject
    SearchPresenter presenter;

    private ActivitySearchBinding binding;
    private HeroAdapter adapter;

    public static Intent newIntent(Context context) {
        return new Intent(context, SearchActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        MyApplication.getInstance().getPresenterComponent().inject(this);
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search);
        presenter.attachView(this);

        setSearchInput();
        setListResults();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(HERO_TAG, adapter.getList());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        //noinspection unchecked
        displayResults((List<Hero>) savedInstanceState.getSerializable(HERO_TAG));
    }

    private void setListResults() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        binding.list.setLayoutManager(linearLayoutManager);

        DividerItemDecoration divider = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        divider.setDrawable(ContextCompat.getDrawable(getBaseContext(), R.drawable.item_divider));
        binding.list.addItemDecoration(divider);

        adapter = new HeroAdapter(
                new ArrayList<>(),
                (hero, itemBinding) -> {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        itemBinding.image.setTransitionName(getString(R.string.image_transition));
                        itemBinding.name.setTransitionName(getString(R.string.name_transition));
                        itemBinding.description.setTransitionName(getString(R.string.description_transition));
                    }

                    Pair<View, String> imagePair = Pair.create(itemBinding.image,
                            getString(R.string.image_transition));
                    Pair<View, String> namePair = Pair.create(itemBinding.name,
                            getString(R.string.name_transition));
                    Pair<View, String> descriptionPair = Pair.create(itemBinding.description,
                            getString(R.string.description_transition));

                    @SuppressWarnings("unchecked") ActivityOptionsCompat options = ActivityOptionsCompat
                            .makeSceneTransitionAnimation(this,
                                    imagePair, namePair, descriptionPair);
                    startActivity(DetailActivity.newIntent(this, hero), options.toBundle());
                }
        );
        binding.list.setAdapter(adapter);
        binding.list.setOnTouchListener((v, event) -> {
            hideSoftKeyboard(this);
            return false;
        });
    }

    private void setSearchInput() {
        binding.search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > SEARCH_LIMIT) {
                    binding.emptyResults.setVisibility(View.GONE);
                    binding.progress.setVisibility(View.VISIBLE);
                    presenter.searchName(s.toString());
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void displayResults(List<Hero> heroes) {
        binding.progress.setVisibility(View.GONE);
        adapter.setList(heroes);
        binding.emptyResults.setVisibility(heroes.size() > 0 ? View.GONE : View.VISIBLE);
    }

    @Override
    public void displayError(String errorMessage) {
        binding.progress.setVisibility(View.GONE);
        Snackbar.make(getCurrentFocus(), errorMessage, Snackbar.LENGTH_LONG).show();
    }
}
