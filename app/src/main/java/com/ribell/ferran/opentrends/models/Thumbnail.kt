package com.ribell.ferran.opentrends.models

import java.io.Serializable

data class Thumbnail(val path: String, private val extension: String) : Serializable {
    override fun toString(): String = path + "." + extension
}