package com.ribell.ferran.opentrends.ui.core;

import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;


public class BasePresenter<V extends BaseMvp.BaseView> implements BaseMvp.BasePresenter<V> {
    protected final Scheduler ioScheduler;

    protected final Scheduler mainScheduler;

    private final CompositeDisposable compositeSubscription = new CompositeDisposable();
    protected V view;

    protected BasePresenter(Scheduler ioScheduler, Scheduler mainScheduler) {
        this.ioScheduler = ioScheduler;
        this.mainScheduler = mainScheduler;
    }

    protected boolean isViewAttached() {
        return view != null;
    }

    @Override
    public void attachView(V view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        compositeSubscription.clear();
        view = null;
    }

    protected void addSubscription(Disposable subscription) {
        this.compositeSubscription.add(subscription);
    }

}