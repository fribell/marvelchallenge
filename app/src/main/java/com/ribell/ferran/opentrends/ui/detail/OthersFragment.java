package com.ribell.ferran.opentrends.ui.detail;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ribell.ferran.opentrends.R;
import com.ribell.ferran.opentrends.databinding.FragmentOthersBinding;
import com.ribell.ferran.opentrends.models.Other;
import com.ribell.ferran.opentrends.ui.detail.adapters.OtherAdapter;

import java.util.ArrayList;
import java.util.List;

public class OthersFragment extends Fragment {
    private static final String RESULTS_TAG = "RESULTS_TAG";
    private FragmentOthersBinding binding;

    private OtherAdapter adapter;

    public static OthersFragment newInstance() {
        return new OthersFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_others, container, false);

        setListResults();
        if (savedInstanceState != null) {
            //noinspection unchecked
            loadData((List<Other>) savedInstanceState.getSerializable(RESULTS_TAG));
        }

        return binding.getRoot();
    }

    private void setListResults() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        binding.list.setLayoutManager(linearLayoutManager);

        DividerItemDecoration divider = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        divider.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.item_divider));
        binding.list.addItemDecoration(divider);

        adapter = new OtherAdapter(new ArrayList<>());
        binding.list.setAdapter(adapter);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putSerializable(RESULTS_TAG, adapter.getList());
        super.onSaveInstanceState(outState);
    }

    public void loadData(List<Other> results) {
        binding.progress.setVisibility(View.GONE);
        adapter.setList(results);
    }
}
