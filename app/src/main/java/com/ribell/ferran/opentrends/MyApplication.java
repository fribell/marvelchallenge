package com.ribell.ferran.opentrends;

import android.app.Application;

import com.ribell.ferran.opentrends.di.components.DaggerPresenterComponent;
import com.ribell.ferran.opentrends.di.components.PresenterComponent;
import com.ribell.ferran.opentrends.di.modules.NetModule;

public class MyApplication extends Application {
    private static MyApplication instance;

    private PresenterComponent presenterComponent;

    @SuppressWarnings("WeakerAccess")
    public MyApplication() {
    }

    public static MyApplication getInstance() {

        if (instance == null) {
            synchronized (MyApplication.class) {
                if (instance == null) {
                    instance = new MyApplication();
                }
            }
        }

        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        setupDagger();
    }

    private void setupDagger() {
        presenterComponent = DaggerPresenterComponent.builder()
                .netModule(new NetModule(BuildConfig.URL, BuildConfig.API_KEY, BuildConfig.PRIVATE_API_KEY))
                .build();
    }

    public PresenterComponent getPresenterComponent() {
        return presenterComponent;
    }
}
