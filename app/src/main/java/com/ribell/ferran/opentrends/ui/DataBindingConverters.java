package com.ribell.ferran.opentrends.ui;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.ribell.ferran.opentrends.R;
import com.ribell.ferran.opentrends.models.Thumbnail;
import com.ribell.ferran.opentrends.utils.GlideApp;


@SuppressWarnings("WeakerAccess")
public class DataBindingConverters {

    @BindingAdapter({"bind:loadImage"})
    public static void loadImage(ImageView view, Thumbnail thumbnail) {
        GlideApp.with(view.getContext())
                .load(thumbnail.toString())
                .override(view.getContext().getResources().getDimensionPixelSize(R.dimen.image_height), view.getContext().getResources().getDimensionPixelSize(R.dimen.image_height))
                .into(view);
    }
}
