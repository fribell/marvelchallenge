package com.ribell.ferran.opentrends.net.interceptors;

import android.support.annotation.NonNull;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class MarvelQueryInterceptor implements Interceptor {

    private final String apiKey;
    private final String privateApiKey;

    public MarvelQueryInterceptor(String apiKey, String privateApiKey) {
        this.apiKey = apiKey;
        this.privateApiKey = privateApiKey;
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request original = chain.request();
        long ts = System.currentTimeMillis();
        final HttpUrl.Builder builder = chain
                .request()
                .url()
                .newBuilder()
                .addQueryParameter("ts", String.valueOf(ts))
                .addQueryParameter("apikey", apiKey)
                .addQueryParameter("hash", stringToMD5(ts + privateApiKey + apiKey));
        HttpUrl url = builder.build();

        Request request = original.newBuilder()
                .header("Content-Type", "application/json")
                .header("Accept", "application/json")
                .url(url)
                .build();
        return chain.proceed(request);
    }

    private static String stringToMD5(String s) {
        MessageDigest m = null;

        try {
            m = MessageDigest.getInstance("MD5");
            m.update(s.getBytes(), 0, s.length());
            return new BigInteger(1, m.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return "";
    }
}
