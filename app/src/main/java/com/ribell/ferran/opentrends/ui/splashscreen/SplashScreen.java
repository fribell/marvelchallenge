package com.ribell.ferran.opentrends.ui.splashscreen;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.ribell.ferran.opentrends.R;
import com.ribell.ferran.opentrends.ui.search.SearchActivity;

import java.util.concurrent.TimeUnit;

public class SplashScreen extends AppCompatActivity {
    private static final long WAITING_TIME_IN_SECONDS = 2;
    private Handler timeoutHandler;
    private Runnable timeoutRunnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        goToSearchScreen();
    }

    private void goToSearchScreen() {
        timeoutHandler = new Handler();
        timeoutRunnable = () -> {
            startActivity(SearchActivity.newIntent(SplashScreen.this));
            finish();
        };
        timeoutHandler.postDelayed(timeoutRunnable, TimeUnit.SECONDS.toMillis(WAITING_TIME_IN_SECONDS));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timeoutHandler.removeCallbacks(timeoutRunnable);
    }
}
