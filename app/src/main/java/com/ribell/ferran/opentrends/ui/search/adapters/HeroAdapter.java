package com.ribell.ferran.opentrends.ui.search.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ribell.ferran.opentrends.R;
import com.ribell.ferran.opentrends.databinding.ItemSearchResultBinding;
import com.ribell.ferran.opentrends.models.Hero;

import java.util.ArrayList;
import java.util.List;

public class HeroAdapter extends RecyclerView.Adapter<HeroAdapter.ViewHolder> {
    private final List<Hero> heroList;
    private final HeroListener listener;

    public HeroAdapter(List<Hero> heroList, HeroListener listener) {
        this.heroList = heroList;
        this.listener = listener;
    }

    @Override
    public HeroAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemSearchResultBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_search_result, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(HeroAdapter.ViewHolder holder, int position) {
        Hero hero = heroList.get(position);
        holder.setHero(hero);

        holder.binding.getRoot().setOnClickListener(v -> listener.onClick(hero, holder.binding));
    }

    @Override
    public int getItemCount() {
        return heroList.size();
    }

    public void setList(List<Hero> heroes) {
        heroList.clear();
        heroList.addAll(heroes);
        notifyDataSetChanged();
    }

    public ArrayList<Hero> getList() {
        return new ArrayList<>(heroList);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final ItemSearchResultBinding binding;

        ViewHolder(ItemSearchResultBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void setHero(Hero hero) {
            binding.setHero(hero);
        }
    }

    public interface HeroListener {
        void onClick(Hero hero, ItemSearchResultBinding view);
    }
}
