package com.ribell.ferran.opentrends.ui.detail;

import com.ribell.ferran.opentrends.models.Other;
import com.ribell.ferran.opentrends.repositoris.MarvelRepository;
import com.ribell.ferran.opentrends.ui.core.BasePresenter;

import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

import static com.ribell.ferran.opentrends.ui.detail.DetailActivity.COMICS;
import static com.ribell.ferran.opentrends.ui.detail.DetailActivity.EVENTS;

public class DetailPresenter extends BasePresenter<DetailMvp.View> implements DetailMvp.Presenter {

    private final MarvelRepository repository;

    public DetailPresenter(MarvelRepository repository, Scheduler ioScheduler, Scheduler mainScheduler) {
        super(ioScheduler, mainScheduler);
        this.repository = repository;
    }

    @Override
    public void loadComicsData(String heroId) {
        repository.loadData(COMICS, heroId)
                .subscribeOn(ioScheduler)
                .observeOn(mainScheduler)
                .subscribe(new SingleObserver<List<Other>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        addSubscription(d);
                    }

                    @Override
                    public void onSuccess(List<Other> results) {
                        if (isViewAttached()) {
                            view.displayComicResults(results);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (isViewAttached()) {
                            view.displayError(e.getMessage());
                        }
                    }
                });
    }

    @Override
    public void loadEventsData(String heroId) {
        repository.loadData(EVENTS, heroId)
                .subscribeOn(ioScheduler)
                .observeOn(mainScheduler)
                .subscribe(new SingleObserver<List<Other>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        addSubscription(d);
                    }

                    @Override
                    public void onSuccess(List<Other> results) {
                        if (isViewAttached()) {
                            view.displayEventsResults(results);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (isViewAttached()) {
                            view.displayError(e.getMessage());
                        }
                    }
                });
    }
}
