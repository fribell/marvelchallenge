package com.ribell.ferran.opentrends.models

import java.io.Serializable

data class Other(var id: String,
                 var title: String,
                 var description: String,
                 var thumbnail: Thumbnail) : Serializable