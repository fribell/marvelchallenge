package com.ribell.ferran.opentrends.models

import java.io.Serializable

const val URL_COMIC: String = "comiclink"
const val URL_DETAIL: String = "detail"
const val URL_WIKI: String = "wiki"

data class Urls(val type: String, val url: String) : Serializable