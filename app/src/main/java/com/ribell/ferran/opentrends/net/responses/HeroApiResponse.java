package com.ribell.ferran.opentrends.net.responses;

import com.ribell.ferran.opentrends.models.Hero;

import java.util.ArrayList;
import java.util.List;

public class HeroApiResponse {

    private DataResponse data;

    private class DataResponse {
        private int count;

        private List<Hero> results;

        public List<Hero> getResults() {
            return results;
        }
    }

    public List<Hero> getResults() {
        return data.count > 0 ? data.getResults() : new ArrayList<>();
    }
}
