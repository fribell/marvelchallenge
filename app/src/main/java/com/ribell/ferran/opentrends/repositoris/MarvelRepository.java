package com.ribell.ferran.opentrends.repositoris;

import com.ribell.ferran.opentrends.models.Hero;
import com.ribell.ferran.opentrends.models.Other;

import java.util.List;

import io.reactivex.Single;

public interface MarvelRepository {
    Single<List<Hero>> searchHero(String name);

    Single<List<Other>> loadData(int type, String heroId);
}
