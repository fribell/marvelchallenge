package com.ribell.ferran.opentrends.ui.detail.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ribell.ferran.opentrends.R;
import com.ribell.ferran.opentrends.databinding.ItemOtherResultBinding;
import com.ribell.ferran.opentrends.models.Other;

import java.util.ArrayList;
import java.util.List;

public class OtherAdapter extends RecyclerView.Adapter<OtherAdapter.ViewHolder> {
    private final List<Other> otherList;

    public OtherAdapter(List<Other> otherList) {
        this.otherList = otherList;
    }

    @Override
    public OtherAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemOtherResultBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_other_result, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(OtherAdapter.ViewHolder holder, int position) {
        Other other = otherList.get(position);
        holder.setOther(other);
    }

    @Override
    public int getItemCount() {
        return otherList.size();
    }

    public void setList(List<Other> list) {
        otherList.clear();
        otherList.addAll(list);
        notifyDataSetChanged();
    }

    public ArrayList<Other> getList() {
        return new ArrayList<>(otherList);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final ItemOtherResultBinding binding;

        ViewHolder(ItemOtherResultBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void setOther(Other other) {
            binding.setOther(other);
        }
    }

}
