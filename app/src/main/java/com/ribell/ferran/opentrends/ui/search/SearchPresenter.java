package com.ribell.ferran.opentrends.ui.search;

import com.ribell.ferran.opentrends.models.Hero;
import com.ribell.ferran.opentrends.repositoris.MarvelRepository;
import com.ribell.ferran.opentrends.ui.core.BasePresenter;

import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

public class SearchPresenter extends BasePresenter<SearchMvp.View> implements SearchMvp.Presenter {

    private final MarvelRepository repository;

    public SearchPresenter(MarvelRepository repository, Scheduler ioScheduler, Scheduler mainScheduler) {
        super(ioScheduler, mainScheduler);
        this.repository = repository;
    }

    @Override
    public void searchName(String name) {

        repository.searchHero(name)
                .subscribeOn(ioScheduler)
                .observeOn(mainScheduler)
                .subscribe(new SingleObserver<List<Hero>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        addSubscription(d);
                    }

                    @Override
                    public void onSuccess(List<Hero> heroes) {
                        if (isViewAttached()) {
                            view.displayResults(heroes);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (isViewAttached()) {
                            view.displayError(e.getMessage());
                        }
                    }
                });

    }
}
