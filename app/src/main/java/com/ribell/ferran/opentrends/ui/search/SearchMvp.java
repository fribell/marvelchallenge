package com.ribell.ferran.opentrends.ui.search;

import com.ribell.ferran.opentrends.models.Hero;
import com.ribell.ferran.opentrends.ui.core.BaseMvp;

import java.util.List;

@SuppressWarnings("WeakerAccess")
public interface SearchMvp extends BaseMvp {

    interface View extends BaseView {
        void displayResults(List<Hero> heroes);

        void displayError(String errorMessage);
    }

    interface Presenter extends BasePresenter<View> {
        void searchName(String name);
    }
}
