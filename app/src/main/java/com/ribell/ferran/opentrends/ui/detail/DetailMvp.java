package com.ribell.ferran.opentrends.ui.detail;

import com.ribell.ferran.opentrends.models.Other;
import com.ribell.ferran.opentrends.ui.core.BaseMvp;

import java.util.List;

@SuppressWarnings("WeakerAccess")
public interface DetailMvp extends BaseMvp {

    interface View extends BaseView {
        void displayComicResults(List<Other> results);

        void displayEventsResults(List<Other> results);

        void displayError(String errorMessage);
    }

    interface Presenter extends BasePresenter<View> {
        void loadComicsData(String heroId);

        void loadEventsData(String heroId);
    }
}
