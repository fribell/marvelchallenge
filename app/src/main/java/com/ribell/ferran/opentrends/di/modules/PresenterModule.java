package com.ribell.ferran.opentrends.di.modules;

import com.ribell.ferran.opentrends.di.scopes.ActivityScope;
import com.ribell.ferran.opentrends.repositoris.MarvelRepository;
import com.ribell.ferran.opentrends.ui.detail.DetailPresenter;
import com.ribell.ferran.opentrends.ui.search.SearchPresenter;

import dagger.Module;
import dagger.Provides;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@Module
public class PresenterModule {


    @ActivityScope
    @Provides
    SearchPresenter provideSearchPresenter(MarvelRepository repository) {
        return new SearchPresenter(repository, Schedulers.newThread(), AndroidSchedulers.mainThread());
    }

    @ActivityScope
    @Provides
    DetailPresenter provideDetailPresenter(MarvelRepository repository) {
        return new DetailPresenter(repository, Schedulers.newThread(), AndroidSchedulers.mainThread());
    }
}
