package com.ribell.ferran.opentrends.di.modules;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ribell.ferran.opentrends.BuildConfig;
import com.ribell.ferran.opentrends.net.interceptors.MarvelQueryInterceptor;
import com.ribell.ferran.opentrends.net.services.MarvelService;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


@Module
public class NetModule {

    private static final int READ_TIME_OUT = 20;
    private static final int CONNECT_TIME_OUT = 5;
    private final String apiUrl;
    private final String apiKey;
    private final String privateApiKey;

    @SuppressWarnings("SameParameterValue")
    public NetModule(String url, String key, String privateKey) {
        this.apiUrl = url;
        this.apiKey = key;
        this.privateApiKey = privateKey;
    }

    //****** INTERCEPTORS ******//
    @Provides
    @Singleton
    HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
        return interceptor;
    }

    @Provides
    @Singleton
    MarvelQueryInterceptor provideMarvelQueryInterceptor() {
        return new MarvelQueryInterceptor(apiKey, privateApiKey);
    }

    //****** OKHTTP ******//
    @Provides
    @Singleton
    OkHttpClient provideBasicOkHttpClient(MarvelQueryInterceptor marvelQueryInterceptor, HttpLoggingInterceptor httpLoggingInterceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(marvelQueryInterceptor)
                .addInterceptor(httpLoggingInterceptor)
                .readTimeout(READ_TIME_OUT, TimeUnit.SECONDS)
                .connectTimeout(CONNECT_TIME_OUT, TimeUnit.SECONDS)
                .build();
    }

    //****** API SERVICES ******//
    @Provides
    @Singleton
    MarvelService provideMarvelService(OkHttpClient okHttpClient, Gson gson) {
        Retrofit retrofit = createDefaultRetrofitInstance(apiUrl, okHttpClient, gson);
        return retrofit.create(MarvelService.class);
    }

    private Retrofit createDefaultRetrofitInstance(String baseUrl, OkHttpClient okHttpClient, Gson gson) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    //****** GSON ******//
    @Provides
    @Singleton
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        return gsonBuilder.create();
    }
}
