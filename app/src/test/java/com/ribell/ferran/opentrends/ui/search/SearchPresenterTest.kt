package com.ribell.ferran.opentrends.ui.search

import com.ribell.ferran.opentrends.TestUtils
import com.ribell.ferran.opentrends.net.responses.HeroApiResponse
import com.ribell.ferran.opentrends.repositoris.MarvelRepository
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations


class SearchPresenterTest {
    @Mock
    private var marvelRepository: MarvelRepository? = null

    @Mock
    private var view: SearchMvp.View? = null

    private lateinit var searchPresenter: SearchMvp.Presenter

    private lateinit var heroApiResponse: HeroApiResponse

    @Before
    @Throws(Exception::class)
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        searchPresenter = SearchPresenter(marvelRepository,
                Schedulers.trampoline(), Schedulers.trampoline())
        searchPresenter.attachView(view)

        heroApiResponse = TestUtils.getHeroes()
    }

    @Test
    fun searchName() {
        val searchWord = "spi"
        Mockito.`when`(marvelRepository!!.searchHero(searchWord)).thenReturn(Single.just(heroApiResponse.results))

        searchPresenter.searchName(searchWord)

        Mockito.verify(view)!!.displayResults(heroApiResponse.results)

        searchPresenter.detachView()
    }

    @Test
    fun searchNameError() {
        val errorMessage = "Test error"
        val searchWord = "spi"
        Mockito.`when`(marvelRepository!!.searchHero(searchWord)).thenReturn(Single.error(Exception(errorMessage)))

        searchPresenter.searchName(searchWord)

        Mockito.verify(view)!!.displayError(errorMessage)

        searchPresenter.detachView()
    }

}