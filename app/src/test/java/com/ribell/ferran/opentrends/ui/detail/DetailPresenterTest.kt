package com.ribell.ferran.opentrends.ui.detail

import com.ribell.ferran.opentrends.TestUtils
import com.ribell.ferran.opentrends.net.responses.OtherApiResponse
import com.ribell.ferran.opentrends.repositoris.MarvelRepository
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

const val COMICS = 0
const val EVENTS = 1

class DetailPresenterTest {

    @Mock
    private var marvelRepository: MarvelRepository? = null

    @Mock
    private var view: DetailMvp.View? = null

    private lateinit var detailPresenter: DetailMvp.Presenter

    private lateinit var comicApiResponse: OtherApiResponse
    private lateinit var eventsApiResponse: OtherApiResponse

    @Before
    @Throws(Exception::class)
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        detailPresenter = DetailPresenter(marvelRepository,
                Schedulers.trampoline(), Schedulers.trampoline())
        detailPresenter.attachView(view)

        eventsApiResponse = TestUtils.getEvents()
        comicApiResponse = TestUtils.getComics()
    }

    @Test
    fun loadComicsData() {
        val heroId = "1009157"
        Mockito.`when`(marvelRepository!!.loadData(COMICS, heroId)).thenReturn(Single.just(comicApiResponse.results))

        detailPresenter.loadComicsData(heroId)

        Mockito.verify(view)!!.displayComicResults(comicApiResponse.results)

        detailPresenter.detachView()
    }

    @Test
    fun loadComicsDataError() {
        val heroId = "1009157"
        val errorMessage = "Comic error"
        Mockito.`when`(marvelRepository!!.loadData(COMICS, heroId)).thenReturn(Single.error(Exception(errorMessage)))

        detailPresenter.loadComicsData(heroId)

        Mockito.verify(view)!!.displayError(errorMessage)

        detailPresenter.detachView()
    }

    @Test
    fun loadEventsData() {
        val heroId = "1009157"
        Mockito.`when`(marvelRepository!!.loadData(EVENTS, heroId)).thenReturn(Single.just(eventsApiResponse.results))

        detailPresenter.loadEventsData(heroId)

        Mockito.verify(view)!!.displayEventsResults(eventsApiResponse.results)

        detailPresenter.detachView()
    }

    @Test
    fun loadEventsDataError() {
        val heroId = "1009157"
        val errorMessage = "Events error"
        Mockito.`when`(marvelRepository!!.loadData(EVENTS, heroId)).thenReturn(Single.error(Exception(errorMessage)))

        detailPresenter.loadEventsData(heroId)

        Mockito.verify(view)!!.displayError(errorMessage)

        detailPresenter.detachView()
    }

}