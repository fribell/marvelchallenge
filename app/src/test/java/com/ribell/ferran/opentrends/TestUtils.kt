package com.ribell.ferran.opentrends

import com.google.gson.Gson
import com.ribell.ferran.opentrends.net.responses.HeroApiResponse
import com.ribell.ferran.opentrends.net.responses.OtherApiResponse
import java.io.InputStreamReader

class TestUtils {
    companion object {
        @Throws(Exception::class)
        fun getHeroes(): HeroApiResponse {
            val `in` = TestUtils::class.java.classLoader.getResourceAsStream("charactersJson.json")
            val reader = InputStreamReader(`in`, "UTF-8")
            val characterResponse = Gson().fromJson<HeroApiResponse>(reader, HeroApiResponse::class.java)
            reader.close()
            return characterResponse
        }

        @Throws(Exception::class)
        fun getComics(): OtherApiResponse {
            val `in` = TestUtils::class.java.classLoader.getResourceAsStream("comicsJson.json")
            val reader = InputStreamReader(`in`, "UTF-8")
            val response = Gson().fromJson<OtherApiResponse>(reader, OtherApiResponse::class.java)
            reader.close()
            return response
        }

        @Throws(Exception::class)
        fun getEvents(): OtherApiResponse {
            val `in` = TestUtils::class.java.classLoader.getResourceAsStream("eventsJson.json")
            val reader = InputStreamReader(`in`, "UTF-8")
            val response = Gson().fromJson<OtherApiResponse>(reader, OtherApiResponse::class.java)
            reader.close()
            return response
        }
    }
}
