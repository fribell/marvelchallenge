package com.ribell.ferran.opentrends.repositoris

import com.ribell.ferran.opentrends.TestUtils
import com.ribell.ferran.opentrends.net.responses.HeroApiResponse
import com.ribell.ferran.opentrends.net.responses.OtherApiResponse
import com.ribell.ferran.opentrends.net.services.MarvelService
import io.reactivex.Single
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Response

const val COMICS = 0
const val EVENTS = 1

class MarvelRepositoryAPITest {

    @Mock
    private var service: MarvelService? = null

    private lateinit var marvelRepository: MarvelRepository

    private lateinit var heroApiResponse: HeroApiResponse
    private lateinit var comicApiResponse: OtherApiResponse
    private lateinit var eventsApiResponse: OtherApiResponse

    @Before
    @Throws(Exception::class)
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        marvelRepository = MarvelRepositoryAPI(service)

        heroApiResponse = TestUtils.getHeroes()
        eventsApiResponse = TestUtils.getEvents()
        comicApiResponse = TestUtils.getComics()
    }

    @Test
    fun searchHero() {
        val name = "spi"
        val response = Response.success(heroApiResponse)
        Mockito.`when`(service!!.searchHero(name)).thenReturn(Single.just(response))

        val result = marvelRepository.searchHero(name).blockingGet()

        Mockito.verify(service)!!.searchHero(name)

        Assert.assertEquals(result.size, heroApiResponse.results.size)
        Assert.assertEquals(result[0].name, heroApiResponse.results[0].name)
    }

    @Test
    fun loadComics() {
        val heroId = "1009157"
        val response = Response.success(comicApiResponse)
        Mockito.`when`(service!!.getComics(heroId)).thenReturn(Single.just(response))

        val result = marvelRepository.loadData(COMICS, heroId).blockingGet()

        Mockito.verify(service)!!.getComics(heroId)

        Assert.assertEquals(result.size, comicApiResponse.results.size)
        Assert.assertEquals(result[0].title, comicApiResponse.results[0].title)
    }

    @Test
    fun loadEvents() {
        val heroId = "1009157"
        val response = Response.success(eventsApiResponse)
        Mockito.`when`(service!!.getEvents(heroId)).thenReturn(Single.just(response))

        val result = marvelRepository.loadData(EVENTS, heroId).blockingGet()

        Mockito.verify(service)!!.getEvents(heroId)

        Assert.assertEquals(result.size, eventsApiResponse.results.size)
        Assert.assertEquals(result[0].title, eventsApiResponse.results[0].title)
    }

}